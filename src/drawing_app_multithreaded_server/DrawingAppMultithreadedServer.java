package drawing_app_multithreaded_server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//import drawing_app_multithreaded_server.model.DrawingCoordinates;
//import drawing_app_client.model.*;
import drawing_coordinates.*;

public class DrawingAppMultithreadedServer {

	private static final int PORT = 2001;
	private static final int MAX_CLIENTS = 10;
	private static ExecutorService client_pool = null;
	private static ServerSocket listening_socket = null;	

	//creating a vector with SingleConnectionHandler objects
	static Vector <SingleConnectionHandler> clients_list = new Vector<>();
	static int index_count = 0;
	public static class SingleConnectionHandler implements Runnable {

		private ObjectOutputStream out_stream = null;
		private ObjectInputStream in_stream = null;
		private final Socket c_socket;
		private Thread client_thread = null;
		private int client_index;

		//constructor
		public SingleConnectionHandler(Socket client_socket, int index){
			this.c_socket = client_socket;
			this.client_index = index;
		}

		@Override
		public void run() {

			// creating input and output streams for a single client
			try {
				out_stream = new ObjectOutputStream(c_socket.getOutputStream());
				in_stream = new ObjectInputStream(c_socket.getInputStream());

			} catch (IOException e) {
				e.printStackTrace();
			}

			//for testing
			this.client_thread = Thread.currentThread();
			System.out.println("Input/Output streams established for thread: " + this.client_thread );

			//getting an input for drawing from the client and sending it to other clients	
			receivingInstructionsFromOneClientandSendingToOthers();
		}
		
		private void receivingInstructionsFromOneClientandSendingToOthers() {
			try {
				while (true) {
					//getting DrawingCoordinates object from the input stream of the drawing client
					DrawingCoordinates coordinates_from_client = (DrawingCoordinates) in_stream.readObject();

					System.out.println("Server: I got some input!  X =  " + coordinates_from_client.getX());
//					System.out.println("Server: I got some input!  COLOR =  " + coordinates_from_client.getColor());

					//sending the DrawingCoordinates object to all clients that are connected 
					for (SingleConnectionHandler client: clients_list) {
						if (client.get_client_index() != this.client_index) {	
							System.out.println("this client's index is: " + client.get_client_index() + "\n");
							client.sendCoordinatesToReceivingClient(coordinates_from_client);
						}
					}
				} 
				
			}catch (ClassNotFoundException | IOException e) {

				System.out.println("\n This thread disconnected: " + this.client_thread + "\n" );
				e.printStackTrace();
				
				// if a client disconnects unexpectedly, will catch the exception and remove it from the list of clients
				clients_list.remove(this);

				try {
					System.out.println("Input stream, outputstream, and socket closed for  : " + this.client_thread );
					this.in_stream.close();
					this.out_stream.close();
					this.c_socket.close();

				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	
		private void sendCoordinatesToReceivingClient(DrawingCoordinates coordinates_from_client) {
			try {
				out_stream.writeObject(coordinates_from_client);
			} catch (IOException e) {
				System.out.println("IN THE sendCoordinatesToReceivingClient METHOD - This client is not connected anymore!");
			}
		}

		public int  get_client_index () {
			return client_index;
		}

	}

	public static void main (String[] arg) {

		System.out.println("Opening server...");
		try {
			// listening socket is waiting for new clients
			listening_socket = new ServerSocket(PORT);	
			System.out.println("Listening for new clients...");

			//creating a thread pool for handling multiple clients
			client_pool = Executors.newFixedThreadPool(MAX_CLIENTS);

			while (true) {

				//this is a blocking method - it waits until a client tries to connect 
				Socket client_socket = listening_socket.accept();

				System.out.println("New client accepted...");

				//opening a new thread for a client
				SingleConnectionHandler new_client = new SingleConnectionHandler(client_socket, index_count);
				client_pool.submit(new_client);
				clients_list.add(new_client);
				index_count++;

			}

		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	protected void finalize() throws IOException {
		listening_socket.close();
	}
}

